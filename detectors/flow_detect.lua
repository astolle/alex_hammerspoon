-- use with: local flow_detect = require("flow_detect")
-- detector for this pomodoro timer: https://flowapp.info/

focus = false
function applicationWatcher(appName, eventType, appObject)
    if (eventType == hs.application.watcher.activated) then
        if (appName == "Flow") then
            if focus == false then
                hs.alert.show("Flow activated")
                focus = true
                hs.execute("slack_status.sh focus", true)
            else
                hs.alert.show("Flow deactivated")
                hs.execute("slack_status.sh none", true)
                focus = false
            end
        end
    end
end
appWatcher = hs.application.watcher.new(applicationWatcher)
appWatcher:start()