-- use with: local lunch_detect = require("lunch_detect")

lunch=false
function lunchWatch(eventType)
    if (eventType == hs.caffeinate.watcher.screensDidLock) then
        -- hs.alert.show("lock")
        local time = hs.timer.localTime()
        if (time > 12*60*60 and time < 14*60*60) then
            hs.alert.show("Lunch Time!")
            lunch=true
            hs.execute("slack_status.sh lunch", true)
        end
    elseif (eventType == hs.caffeinate.watcher.screensDidUnlock) then    
        -- hs.alert.show("unlock")
        if lunch == true then
            lunch=false
            hs.alert.show("Deactivate lunch state!")
            hs.execute("slack_status.sh none", true)
        end
    end
end
lunchWatcher = hs.caffeinate.watcher.new(lunchWatch)
lunchWatcher:start()