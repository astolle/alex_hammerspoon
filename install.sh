#!/bin/sh
#
#set -x

HS_PATH="$HOME/.hammerspoon"

test -r $HS_PATH || {
    echo "err: hammerspoon config path \"$HS_PATH\" not readable. Not installed?"
    exit 0
}

# create symlink for every script to hammerspoon config
for file in detectors/*.lua;
do
    # source to link
    ln -f -s $(pwd)/$file $HS_PATH/$(basename $file)
done

echo "info: don't forget to edit your $HS_PATH/init.lua to activate a script"