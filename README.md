# Alex Hammerspoon Detectors

## About

inspired by: <https://ruk.ca/content/automatically-setting-my-slack-status-when-im-zoom-meeting>

## Installation

Tested on MacOS X.

### prerequisites

* install [Slack Status Updater](https://github.com/mivok/slack_status_updater) to update slack status
* install [Hammerspoon](https://www.hammerspoon.org/)
* enable "Accessibility" in Hammerspoon preferences

### install

* clone the code
* create symlinks in hammerspoon config

```sh
git clone https://gitlab.com/astolle/alex_hammerspoon.git
cd alex_hammerspoon
./install.sh
```

* edit `.hammerspoon/init.lua` and add the detectors you want

```lua
local flow_detect = require("flow_detect")
local lunch_detect = require("lunch_detect")
```

* reload hammerspoon config
